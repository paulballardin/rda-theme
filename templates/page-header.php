<?php use Roots\Sage\Titles; 

$banner_image = get_field('page_header_banner', 'option');
$banner_image_url = $banner_image['url'];
$style =  sprintf('style="background-image:url(%s);"', $banner_image_url);

?>

<div class="page-header" <?php if($style) { _e($style); }?>>
	<div class="container">
		<h1><?  Titles\title(); ?></h1>
	</div>
</div>
