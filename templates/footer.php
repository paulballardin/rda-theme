<footer class="content-info">
  <div class="container">
  	<div class="row">
  		<div class="col-sm-2 footer-col-1">
  			<p><a href="/" title="RDA Surf Sports Coaching">
  				<img class="img-responsive" src="<?php echo esc_url(get_stylesheet_directory_uri()) . '/dist/images/logo-white.png'?>" alt="<?php bloginfo('description'); ?>">
  			</a>
  			</p>
  			<p class=" lead col-sm-6"><a class="phone-mobile" href="tel:61413310716"><i class="fa fa-phone-square"></i> +61 413310716</a></p>
  			<p class=" lead col-sm-6"><a href="mailto:rick.duncan@iinet.net.au"><i class="fa fa-envelope-o"></i> rick.duncan@iinet.net.au</a></p>
  		</div>
		<div class="col-sm-2 hidden-xs">
			<?php
	        if (has_nav_menu('footer_navigation_1')) :
	          wp_nav_menu(['theme_location' => 'footer_navigation_1']);
	        endif;
        ?>
		</div>
		<div class="col-sm-2 hidden-xs">
			<?php
	        if (has_nav_menu('footer_navigation_2')) :
	          wp_nav_menu(['theme_location' => 'footer_navigation_2']);
	        endif;
        ?>
		</div>
		<div class="col-sm-2 hidden-xs">
			<?php
	        if (has_nav_menu('footer_navigation_3')) :
	          wp_nav_menu(['theme_location' => 'footer_navigation_3']);
	        endif;
        ?>
		</div>
		<div class="col-sm-2 hidden-xs">
			<?php
	        if (has_nav_menu('footer_navigation_4')) :
	          wp_nav_menu(['theme_location' => 'footer_navigation_4']);
	        endif;
        ?>
		</div>
		<div class="col-sm-2 hidden-xs">
			<?php
	        if (has_nav_menu('footer_navigation_5')) :
	          wp_nav_menu(['theme_location' => 'footer_navigation_5']);
	        endif;
        ?>
		</div>
  	</div>
    
  </div>
</footer>
<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">

				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>
