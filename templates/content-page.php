<?php if (has_post_thumbnail()) { $thumb = get_the_post_thumbnail_url(); } ?>
	<div class="<?php if ($thumb){ _e('col-sm-9'); } else { _e('col-sm-12'); }?>">
     	<?php the_content(); ?>
	</div>
<?php if ($thumb) { ?>
<div class="col-sm-3">
  <?php the_post_thumbnail('page-thumb', array( 'class' => 'img-responsive' )); ?>    
</div>
	<?php } ?>       

<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
