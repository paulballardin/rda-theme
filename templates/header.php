
<header class="banner " role="banner">
  
  <div class="navbar navbar-default container">
    <div class="navbar-header ">    
      <button type="button" class=" navbar navbar-default navbar-toggle collapsed " data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo esc_url(get_stylesheet_directory_uri()) . '/dist/images/logo.png'?>" alt="<?php bloginfo('description'); ?>"></a>
    </div>
    <nav class="collapse navbar-collapse navbar-right" role="navigation">
        <?php
        if (has_nav_menu('utility_navigation')) :
          wp_nav_menu(['theme_location' => 'utility_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
    </nav>
    <nav class="collapse navbar-collapse navbar-right" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav ']);
      endif;
      ?>
    </nav>
  </div>
</header>
