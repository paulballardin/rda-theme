<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
* Add ACF Theme Options Page
*/

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page();
  
}

/**
* Add Excrpts to Pages
*/
add_action( 'init', __NAMESPACE__ . '\\my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/** 
* Add pay link to utility navigation
 */

add_filter( 'wp_nav_menu_items', __NAMESPACE__ . '\\your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {
    if ($args->theme_location  == 'utility_navigation') {
        $items .= '<li id="menu-item-150" class="stripe-amount menu-item menu-item-type-custom menu-item-object-custom menu-item-150"><a title="May Payment" href="/make-payment-page" ><span class="glyphicon Pay Fees"></span>&nbsp;<i class="fa fa-credit-card"></i> Pay Fees</a></li>';
    }
    return $items;
}




