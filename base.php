<?php
namespace Roots\Sage\Base;
use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDR5B9"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <?php if (!is_home()){
          get_template_part('partials/stripes');
          } else if ( get_option( 'page_for_posts' ) ) {
            $posts_page_id = get_post( get_option( 'page_for_posts' ) );
            echo apply_filters( 'the_content', $posts_page->post_content );
            get_template_part('partials/stripes');
          } else { ?>
            <div class="wrap container" role="document">
              <div class="content row">
                <main class="main">
                  <?php include Wrapper\template_path(); ?>
                </main><!-- /.main -->
              </div><!-- /.content -->
            </div><!-- /.wrap -->

       <?php     } ?>
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
