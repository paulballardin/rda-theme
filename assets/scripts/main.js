/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

          // Loads Typekit
          function loadTypeKits(d) {
            var config = {
              kitId: 'nie2atq',
              scriptTimeout: 3000,
              async: true
            },
            h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!=="complete"&&a!=="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config);}catch(e){}};s.parentNode.insertBefore(tk,s);
            
          }
          loadTypeKits(document);

          $('.equalHeight').matchHeight();
        
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        
        // // Payment popover


        //     $(".stripe-amount a").popover({
        //         title: 'Enter amount to pay',
        //         content: '<form class="form-group"><input class="form-control" type="number" name="payment-amount" id="payment-amount"><a role="button" id="submit-amount" class="form-control btn btn-primary" value="submit">Submit</a></form>',
        //         html: true,
        //         placement: 'bottom' 


        //     });



        // // Stripe Checkout ref https://stripe.com/docs/checkout#integration-custom

        //   var handler = StripeCheckout.configure({
        //     key: 'pk_live_xTdCQRmFCmHy5zRwUeyzBfHk',
        //     //image: '/img/documentation/checkout/marketplace.png',
        //     locale: 'auto',
        //     token: function(token) {
        //       // You can access the token ID with `token.id`.
        //       // Get the token ID to your server-side code for use.
        //     }
        //   });

          
        //   $(document).on( 'shown.bs.popover', function(){

        //       $('#submit-amount').on('click', function(e) { 

        //         $('.stripe-amount a').popover('hide')


        //         var pmt_amount = $('input#payment-amount').val() * 100;
        //           handler.open({
        //             name: 'RickDuncanAcademy.com',
        //             description: 'Fees Payment',
        //             amount: pmt_amount,
        //             zipCode: true,
        //             currency: 'aud',
        //             billingAddress: true
        //           });
        //           e.preventDefault();
                  
        //       });

        //   } );



        //   // $('#submit-amount').on('click', function(e) {
        //   //   // Open Checkout with further options:

        //   //   var pmt_amount = $('input#payment-amount').val();

        //   //   console.log(amount);

        //   //   handler.open({
        //   //     name: 'RickDuncanAcademy.com',
        //   //     description: 'Fees Payment',
        //   //     amount: pmt_amount,
        //   //     zipCode: true
        //   //   });
        //   //   e.preventDefault();
        //   // });

        //   // Close Checkout on page navigation:
        //   $(window).on('popstate', function() {
        //     handler.close();
        //   });

       


      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
