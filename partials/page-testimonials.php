

<?php  // WP_Query arguments for custom post type - testimonial

$args = array (
	'post_type'              => array( 'testimonial' ),
	'orderby'                => 'rand',
	'posts_per_page' 		=> '1',
);

// The Query
$query = new WP_Query( $args );
if ( $query->have_posts() ) {	
	while ( $query->have_posts() ) {
		$query->the_post();
// The Loop
		$banner_image = get_sub_field('banner_image');
		$banner_image_url = $banner_image['url'];
		$style =  sprintf('style="background-image:url(%s);"', $banner_image_url);

		$thumb = '';
		if (has_post_thumbnail()) {
			$banner_image_url = get_the_post_thumbnail_url();
			$style =  sprintf('style="background-image:url(%s);"', $banner_image_url);
			//get_the_post_thumbnail();
		}

?>
<section class="stripe testimonial" <?php if($style){_e($style);}?>> 
	<div class="container">
		<div class="row">
			<div class="testimonial-wrapper col-sm-10 col-sm-push-1 text-center ">
				<div class="testimonial-content">
					<?php the_content(); ?>
				</div>
				<div class="attribute-wrapper">
					<p><cite><?php echo get_post_meta( get_the_ID(), 'cite', true ); ?><br>
					<?php echo get_post_meta( get_the_ID(), 'title', true ); ?></cite></p>
				</div>
			</div>			
		</div>
	</div>
</section>
<?php
	} // Endwhile

} else {
	// no posts found
}

// Restore original Post Data
wp_reset_postdata();

?>

