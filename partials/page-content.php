<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
use Roots\Sage\Titles; 

$banner_image = get_field('page_header_banner', 'option');
$banner_image_url = $banner_image['sizes']['page-header'];
$style =  sprintf('style="background-image:url(%s);"', $banner_image_url);

?>

<section class="stripe default">
  <div class="page-header" <?php if($style) { _e($style); }?>>
  	<div class="container">
  		<h1 class="page-banner-content"><?= Titles\title(); ?></h1>
  	</div> 
  </div>
  <div class="wrap container" role="document">
    <div class="content row">
        <?php include Wrapper\template_path(); ?>
    </div><!-- /.content -->
  </div><!-- /.wrap -->
</section>


