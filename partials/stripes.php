<?php 

use Roots\Sage\Stripes;
if(is_home()) {
	$page_id = get_option( 'page_for_posts' ); 
} else {
	$page_id = get_the_ID();
}


if( have_rows('layouts', $page_id) ):
	
    while ( have_rows('layouts', $page_id) ) : the_row();
		
		//Page Content	    
	    if( get_row_layout() == 'content' ):
	    	get_template_part('partials/page', 'content');
	    endif;

		//Page Banners	    
	    if( get_row_layout() == 'banner' ):
	    	get_template_part('partials/page', 'banner');
	    endif;

	    //Call to action 
	    if( get_row_layout() == 'call_to_action' ):
	    	get_template_part('partials/page', 'call-to-action');
	    endif;

	    //Additional Content   
	    if( get_row_layout() == 'additional_content' ):
	    	get_template_part('partials/page', 'additional-content');
	    endif;

	    //Feature boxes    
	    if( get_row_layout() == 'feature_boxes' ):
	    	get_template_part('partials/page', 'feature-boxes');
	    endif;

	   	//Testimonials    
	    if( get_row_layout() == 'testimonials' ):
	    	get_template_part('partials/page', 'testimonials');
	    endif;

	    //Sub pages    
	    if( get_row_layout() == 'sub_pages' ):
	    	get_template_part('partials/page', 'sub-pages');
	    endif;

	endwhile;
endif;	

if (is_archive() || is_single() ){
	get_template_part('partials/page', 'content');
	get_template_part('partials/page', 'call-to-action');
	get_template_part('partials/page', 'testimonials');
}

 ?>