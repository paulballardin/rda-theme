
<?php 


$banner_image = get_sub_field('banner_image');
$banner_image_url = $banner_image['url'];
$style =  sprintf('style="background-image:url(%s);"', $banner_image_url);
$banner_text = get_sub_field('banner_text');
$banner_button_link = get_sub_field('banner_button_link');
$banner_url = get_sub_field('banner_url');
$banner_button_label = get_sub_field('banner_button_label');



 ?>
<section class="page-banner text-center" <?php if (!empty($banner_image)){ echo $style;} ?>> 
		<div class="page-banner-content">
			
			<?php if (!empty($banner_text)): ?>
				<p><?php _e($banner_text) ?></p>
			<?php endif ?>
			

			<?php if (!empty($banner_button_link)): ?>
				<a class="btn btn-default btn-lg" href="<?php _e($banner_button_link) ?>" ><?php 
					if(!empty($banner_button_label)){
						_e($banner_button_label);
					}
					else
					{
						_e("Find out more");
					}
				?></a>
			<?php endif ?>
		
		</div>
</section>
