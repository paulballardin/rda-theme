<?php 

$section_heading = get_sub_field('section_heading');
$section_content = get_sub_field('section_content');
?>


<section class="stripe feature-boxes" <?php if (!empty($banner_image)){ echo $style;} ?>> 
	<div class="container">
	
	<?php if (!empty($section_heading)) {?>
		<h2 class="text-center"><?php echo $section_heading; ?></h2>
	<?php } ?> 

	<?php if (!empty($section_content)) {?>
		<?php echo $section_content; ?>
	<?php } ?> 
	
	<?php if( have_rows('features') ){ ?>
		<div class="features-row row row-centered" >
		<?php while ( have_rows('features') ) : the_row(); ?>

			<div class="feature-wrapper col-sm-3 col-centered">
				<div class="feature text-center">
					<div class="feature-content equalHeight"><?php 

					$feature_title = get_sub_field('feature_title');
					$feature_content = get_sub_field('feature_content');
					$feature_button_url = get_sub_field('feature_button_url');
					$feature_button_label = get_sub_field('feature_button_label');
					$feature_icon = get_sub_field('feature_icon');
					
					?>
					<?php if(!empty($feature_icon)) { ?>
						<?php if(!empty($feature_button_url)) { ?><a  href="<?php _e($feature_button_url); ?>"><?php } ?><object><?php _e($feature_icon); ?></object><?php if(!empty($feature_button_url)) { ?></a><?php } ?>
					<?php } ?>


					<?php if(!empty($feature_title)) { ?>
						<h3><?php _e($feature_title); ?></h3>
					<?php } ?>
					

					<?php if(!empty($feature_content)) { ?>
						<?php _e($feature_content); ?>
					<?php } ?>
						</div>
					<?php if(!empty($feature_button_url)) { ?>
						<a class="btn btn-primary " href="<?php _e($feature_button_url); ?>"><?php if(!empty($feature_button_label)) { _e($feature_button_label); } else { _e('Learn More'); }?></a>
					<?php } ?>
				</div>
			</div>

		<?php endwhile; ?> 
		</div>
	<?php } ?>
	</div>
</section>