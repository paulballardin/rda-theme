<?php 


$call_to_action_text = get_field('call_to_action_text', 'option');
if (empty($call_to_action_text)) {
	$call_to_action_text = 'Contact Rick today to find out about how he can help you be the best athlete you can be';
}

$call_to_action_button_label = get_field('call_to_action_button_label', 'option');
if (empty($call_to_action_button_label)) {
	$call_to_action_button_label = 'Enquire now';
}

$call_to_action_url = get_field('call_to_action_url', 'option');
if (empty($call_to_action_url)) {
	$call_to_action_url = '#';
}

?>

<section class="stripe call-to-action" > 
	<div class="container">
		<div class="row">
			<div class="cta-wrapper table">
				<div class="col-sm-8 equalHeight">
					<div class="cta-text">
						<?php _e($call_to_action_text); ?>
					</div>
				</div>
				<div class="col-sm-4 equalHeight ">
					<div class="cta-button-block">
						<a href="<?php _e($call_to_action_url); ?>" class="btn btn-default btn-lg" > <?php _e($call_to_action_button_label)	; ?></a>
					</div>
				</div>
			</div>
		</div>	

	</div>
</section>